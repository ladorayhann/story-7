from django.urls import path
from . import views

app_name = "jspage"

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.loginView, name='login'),
    path('profile/', views.index, name='index'),
    path('booksfinder/', views.books, name='books'),
    path('logout/', views.logoutView, name='logout')

]
