from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import json
import time
from jspage.views import index, books
from selenium.webdriver.chrome.options import Options
from .views import *;
from selenium import webdriver

class UnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.home = reverse('jspage:home')
        self.index = reverse('jspage:index')
        self.books = reverse('jspage:books')
        self.authpage_login = reverse("jspage:login")


    def test_url_exist(self):
        response1 = Client().get(self.index)
        response2 = Client().get(self.books)
        response3 = Client().get(self.home)
        response4 = Client().get(self.authpage_login)
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response3.status_code, 200)
        self.assertEqual(response4.status_code, 200)


    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response1 = Client().get(self.index)
        response2 = Client().get(self.books)
        response3 = Client().get(self.home)
        response4 = Client().get(self.authpage_login)

        self.assertTemplateUsed(response1, 'jspage.html')
        self.assertTemplateUsed(response2, 'books.html')
        self.assertTemplateUsed(response3, 'home.html')
        self.assertTemplateUsed(response4, 'login.html')




class TestFunctionalPage(StaticLiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage') 
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctionalPage, self).setUp()

    def tearDown(self):

        self.browser.quit()
        super(TestFunctionalPage, self).tearDown()


    def test_element_in_project(self):
        self.browser.get(self.live_server_url + '/booksfinder')


        alert = self.browser.find_element_by_class_name("head")
        self.assertEquals(

                alert.text,
                'Book Finder'

        )
        time.sleep(10)

    def test_accordion(self):
        self.browser.get(self.live_server_url + "/profile")
        content = self.browser.find_element_by_class_name("inside")
        alert = self.browser.find_element_by_class_name("panels")
        alert.click()
        self.assertIn("Student", self.browser.page_source)


    def test_ajax(self):
        self.browser.get(self.live_server_url + "/booksfinder")
        element = self.browser.find_element_by_id("search")
        element.send_keys("harry potter")
        submit = self.browser.find_element_by_id("button")
        submit.click()
        time.sleep(10)
        assert "Harry Potter" in self.browser.page_source

    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url)

        title = self.browser.find_element_by_tag_name('h3')
        self.assertIn("Selamat Datang", title.text)
        time.sleep(10)

    def test_can_login(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.assertIn("Selamat Datang najib", self.browser.page_source)
        time.sleep(10)




    def test_can_signup(self):
        self.browser.get(self.live_server_url + "/login")

        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('najib')
        email.send_keys('najib@mail.com')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.assertIn("Selamat Datang najib", self.browser.page_source)
        time.sleep(10)

    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(10)

        self.assertIn("Selamat Datang di Website Kami", self.browser.page_source)
        time.sleep(10)

    def test_login_with_wrong_username(self):
       self.browser.get(self.live_server_url + "/login")



       username = self.browser.find_element_by_id('username_login')
       password = self.browser.find_element_by_id('password_login')
       submit = self.browser.find_element_by_id('submit_login')

       username.send_keys('najib')
       password.send_keys('test123')
       submit.click()
       time.sleep(10)
       self.authpage_login = reverse("jspage:login")
       response4 = Client().get(self.authpage_login)



       self.assertTemplateUsed(response4, 'login.html')
       time.sleep(10)

    def test_signup_with_same_username(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")



        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('najib')
        email.send_keys('najib@mail.com')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)
        self.authpage_login = reverse("jspage:login")
        response4 = Client().get(self.authpage_login)



        self.assertTemplateUsed(response4, 'login.html')

        time.sleep(10)

    def test_auth_cannot_access_login(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.browser.get(self.live_server_url + "/login")

        self.assertIn("Selamat Datang najib", self.browser.page_source)
        time.sleep(10)
