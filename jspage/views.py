from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User




def index(request):
    return render(request, 'jspage.html')

def books(request):
    return render(request, 'books.html')

def home(request):
    context = {
        'greeting' : 'Selamat Datang di Website Kami',
        'phase' : 'login',
    }

    if request.user.is_authenticated:

        context['greeting'] = 'Selamat Datang ' + request.session["username"]
        context['phase'] = 'logout'


    return render(request, 'home.html', context)



def loginView(request):

    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('jspage:home')
        else:
            return render(request, 'login.html')

    if request.method == "POST":
        if len(request.POST) == 3:
            username = request.POST["username"]
            password = request.POST["password"]


            user = authenticate(username=username, password=password)



            if user is not None:
                login(request, user)
                request.session["username"] = user.username
                return redirect('jspage:home')
            else:
                return redirect('jspage:login')

        elif len(request.POST) == 4:
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']

            user = User.objects.create_user(username=username, email=email, password=password)
            login(request, user)
            request.session["username"] = user.username
            return redirect('jspage:home')


    # print(request.user)
    #
    # username_1 = "saya"
    # password_1 = "saya"
    # user = authenticate(request, username=username_1, password=password_1)
    # login(request, user)


@login_required
def logoutView(request):
    request.session.flush()
    if request.method == "GET":

        if request.user.is_authenticated:
            logout(request)
        return redirect('jspage:home')

    # print(request.user)
    #
    # username_1 = "saya"
    # password_1 = "saya"
    # user = authenticate(request, username=username_1, password=password_1)
    # login(request, user)
